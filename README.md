# Corona Infinario module

## You need an account

First of all visit [infinario.com](https://infinario.com/) and create account here.

## How to use this module

* Load and initialize Infinario in `main.lua` with your Infinario project ID:

```lua
infinario = require( 'infinario' )
infinario:init( '1247d31c-c247-11e4-a445-b083fedeed2e' )
```

* Send events:

```lua
	infinario:track('EVENT_TYPE', {key1 = 'value1', key1 = 'value1'})
```

* Examples:

```lua
function scene:enterScene( event )
	-- track how player moves in your game
	infinario:track('enter_scene', {
		scene_name = 'menu'
		-- put all useful data here
	})

	local group = self.view
	-- rest of code
end

function start_game()
	-- track custom event: player starts game
	infinario:track('start_game', {
		level = 1,
		lives = 3,
		try = 2,
		-- put all useful data here
	})
end

function stop_game()
	-- track custom event: game over
	infinario:track('stop_game', {
		level = 1,
		lives = 0,
		try = 2,
		score = 123,
		-- put all useful data here
	})
end
```

## Setting player properties

Player is identified by uniq ID.

If you don't provide this ID, Infinario will use ID of device.

If you have real player ID (returned from [game network](http://docs.coronalabs.com/daily/api/library/gameNetwork/index.html) for example) use it to identify player.

```lua
-- send real player ID to Infinario
infinario:identify( PLAYER_ID )

-- and send some custom params 
infinario:update({
	alias = 'Player Alias',
	-- put all useful data here
})
```

## Documentation

Find more details about Infinario API on [guides.infinario.com](http://guides.infinario.com/technical-guide/rest-client-api/) page.

## Issues

* cache data when network is not available